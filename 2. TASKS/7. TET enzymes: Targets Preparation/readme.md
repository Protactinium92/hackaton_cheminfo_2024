# Task 7: TET Enzymes - Target Preparation

## GROUP 7: TET - enzymes
- Nguyen
- Mirandela
- Kennedy
- Mqawass
- **Main coordinator:** Dragos Horvath


## Overview
This task focuses on the preparation of targets for TET enzymes by accessing various structural databases, using specialized software, and performing a series of tasks aimed at collecting, documenting, and refining structural data. The goal is to ensure that the target structures are ready for downstream applications such as docking studies and molecular dynamics simulations.

## Access to Databases
- **PDB** (Protein Data Bank)
- **UniProt**

## Software
- **MOE** (Molecular Operating Environment)
- **Schrödinger**
- **CMDock**
- **YASARA**
- **AlphaFold**
- **Modeler**
- **SwissModel**

## Tasks

### Collect Structural Data
- Gather structural data of TET enzymes from PDB and UniProt databases.

### Document Structural Data
- Document all collected structural data.
- Include bibliographic sources and PDB files.
- Download and store relevant articles for reference.

### Define All Chemical Entities in Structural Data
- Identify and define all chemical entities present in the structural data.

### Prepare the Structural Data
- **Complete Missing Parts of the Structure**: Use software tools to model and complete any missing parts of the enzyme structures.
- **Add Hydrogens**: Ensure all hydrogen atoms are added to the structures.
- **Chemical Equilibrium**: Adjust structures for chemical equilibrium, including tautomers and protonation states.
- **Solvation**: Prepare the structures for solvation studies.

### Molecular Dynamics
- Perform molecular dynamics simulations to study the behavior of the enzyme structures over time.

### (Re-)Docking
- **Pocket Detection**: Identify potential binding pockets on the enzyme structures.
- **Blind Docking**: Perform blind docking to explore potential binding sites.
- **Cross-Docking**: Conduct cross-docking studies to compare binding affinities across different enzyme structures.

## Deliverables
1. **Collected Structural Data**
   - A comprehensive collection of structural data for TET enzymes.
2. **Documentation**
   - Thorough documentation of the structural data, including sources and PDB files.
3. **Prepared Structures**
   - Fully prepared and refined enzyme structures, ready for downstream applications.
4. **Molecular Dynamics Simulations**
   - Results from molecular dynamics simulations.
5. **Docking Studies**
   - Results from pocket detection, blind docking, and cross-docking studies.

## Example Software Uses
- **MOE** and **Schrödinger** for structural refinement and docking studies.
- **CMDock** for docking simulations.
- **YASARA** for molecular dynamics.
- **AlphaFold**, **Modeler**, and **SwissModel** for modeling and completing missing parts of structures.

This task aims to create a robust and comprehensive dataset of TET enzyme structures, ensuring the data is well-organized, documented, and prepared for future research and analysis.
